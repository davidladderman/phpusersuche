<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">

    <title>Benutzerdetails</title>
</head>
<body>
<div class="container">


    <h1 class="mt-5 mb-3">Benutzerdetails</h1>
    <a href="../index.php">Zurück</a>
    <p>&nbsp;</p>
    <?php
    require "../lib/function.php";

    $vName = '';
    $nName = '';
    $date = '';
    $mail = '';
    $tel = '';
    $street = '';


    $id = $_GET['id'];

    $result = getDataPerID($id);
    while($row = mysqli_fetch_array($result)){
        //zum date umwandeln
        $orgDate = $row[5];
        $newDate = date("d.m.Y", strtotime($orgDate));


        $vName = $row[1];
        $nName = $row[2];
        $date = $newDate;
        $mail = $row[3];
        $tel = $row[4];
        $street = $row[6];
    }
    //echo $result->fetch_assoc()['id'];

    ?>
    <table class="table">
        <tbody>
        <tr>
            <th scope="row">Vorname</th>
            <td><?=$vName?></td>
        </tr>
        <tr>
            <th scope="row">Nachname</th>
            <td><?= $nName?></td>
        </tr>
        <tr>
            <th scope="row">Geburtsdatum</th>
            <td><?=$date?></td>
        </tr>
        <tr>
            <th scope="row">E-Mail</th>
            <td><?= $mail?></td>
        </tr>
        <tr>
            <th scope="row">Telefon</th>
            <td><?= $tel?></td>
        </tr>
        <tr>
            <th scope="row">Straße</th>
            <td><?= $street?></td>
        </tr>
        </tbody>
    </table>



</div>


</body>
</html>