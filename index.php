<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Benutzer</title>
</head>
<body>
<div class="container">
    <h1 class="mt-5 mb-3">Benutzerdaten anzeigen</h1>
    <?php
    //Reinladen der functionen
    $search = '';

    if (isset($_POST['search'])){
        $search = isset($_POST['search'])? $_POST['search'] : '';
    }

    require "lib/function.php";

    ?>
    <form id="form_search" action="index.php" method="post">
        <div class="form-row">
            <label for="search" class="col-sm-1 col-form-label">Suche: </label>
            <div class="col-sm-4">
                <input type="text"
                       name="search"
                       class="form-control"
                       value="<?=htmlspecialchars($search)?>"
                       required
                />
            </div>
            <div class="col-sm-2">
                <input type="submit"
                       name="submit"
                       class="btn btn-primary btn-block"
                       value="Suchen"/>
            </div>
            <div class="col-sm-2">
                <a href="index.php" class="btn btn-secondary btn-block">Leeren</a>
            </div>
        </div>
    </form>
</div>
<p>&nbsp;</p>
<div class="container">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Geburtsdatum</th>
        </tr>
        </thead>
        <tbody>
        <?php
        //Zuerst Variable machen weil wenn man die methode mitgeht als parameter ensteht eine endlosschleife weil es immer nur den ersten wert zurück gibt

        if (isset($_POST['submit'])) {
            $lol = getFiltered($search);
            if(mysqli_fetch_array($lol)==NULL){
                echo "<div class='alert alert-danger'><p>Keine Einträge gefunden. Bitte erneut versuchen!</p><ul>";
            }
            while ($row = mysqli_fetch_array($lol)) {
                $id = $row[0];
                $orgDate = $row[5];
                $newDate = date("d.m.Y", strtotime($orgDate));
                echo "<tr>
                            <td><a href='pages/userdetail.php?id=$id'>" . $row[1] . " " . $row[2] . "</a></td>
                            <td>" . $row[3] . "</td>
                            <td>" . $newDate . "</td>
                    </tr>";
            }

        } else {
            $lol = getAllData();
            while ($row = mysqli_fetch_array($lol)) {
                $id = $row[0];
                $orgDate = $row[5];
                $newDate = date("d.m.Y", strtotime($orgDate));
                echo "<tr>
                                <td><a href='pages/userdetail.php?id=$id'>" . $row[1] . " " . $row[2] . "</a></td>
                                <td>" . $row[3] . "</td>
                                <td>" . $newDate . "</td>
                      </tr>";
            }
        }
        ?>
        </tbody>
    </table>
</div>

</body>
</html>